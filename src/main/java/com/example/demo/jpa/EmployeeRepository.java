package com.example.demo.jpa;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface EmployeeRepository 
	extends PagingAndSortingRepository<Employee, Long>, 
			JpaSpecificationExecutor<Employee>,
			QueryDslPredicateExecutor<Employee> {
	
	List<Employee> findByFirstNameContainsIgnoreCase(String firstName);

	List<Employee> findByAddressStreetContainsIgnoreCase(String street);

	@Query("select e from Employee e where e.address.street like %?1%")
	List<Employee> findByAddressCustomQuery(String street);

	@Query(value = "select * from employee e join address a on e.address_id = a.id where a.street like %?1%", nativeQuery = true)
	List<Employee> findByAddressCustomNativeQuery(String street);
	
	EmployeeNameProjection findOneEmployeeNameProjectionById(Long id);

	EmployeeNameDTO findOneEmployeeNameDTOById(Long id);

	<T> T findOneDynamicEmployeeProjectionById(Long id, Class<T> type);

	<T> T findOneByUsername(String username, Class<T> type);
}
