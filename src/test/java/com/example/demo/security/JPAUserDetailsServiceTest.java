package com.example.demo.security;


import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.example.demo.jpa.EmployeeRepository;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = SecureRestController.class)
@Import({JPAUserDetailsConfiguration.class, MultiHttpSecurityConfig.class})
public class JPAUserDetailsServiceTest {
	
	@Autowired
	private MockMvc mvc;
	
	@MockBean
	private EmployeeRepository repository;
	
	@Test
	public void testUnauthorized() throws Exception {
		mvc.perform(
				get("/secure")
			)
			.andExpect(status().isUnauthorized());
	}

	@Test
	public void testWithBasicAuth() throws Exception {
		UserDetails user = User.withUsername("user").password("password").roles("USER").build();
		
		when(repository.findOneByUsername("user", UserDetails.class))
			.thenReturn(user);
		
		mvc.perform(
				get("/secure/user")
				.with(httpBasic("user", "password"))
			)
			.andExpect(status().isOk())
			.andExpect(content().string("user"));
		
		verify(repository).findOneByUsername("user", UserDetails.class);
	}

}
