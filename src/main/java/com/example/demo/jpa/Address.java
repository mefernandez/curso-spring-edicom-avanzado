package com.example.demo.jpa;

import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Entity
public class Address {
	
	@Id
	@GeneratedValue
	private UUID id;
	//private Long id;
	
	private String street;

}
