package com.example.demo.integration;


import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.handler.LoggingHandler;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.integration.LogGatewayTest.Configuration.MyGateway;

@RunWith(SpringRunner.class)
public class LogGatewayTest {
	
	@TestConfiguration
	@EnableIntegration
	@IntegrationComponentScan
	public static class Configuration {
	
	    @Bean
	    public MessageChannel logInputChannel() {
	        return new DirectChannel();
	    }
	    
	    @Bean
	    @ServiceActivator(inputChannel = "logChannel")
	    public LoggingHandler logging() {
	        LoggingHandler adapter = new LoggingHandler(LoggingHandler.Level.DEBUG);
	        adapter.setLoggerName("TEST_LOGGER");
	        adapter.setLogExpressionString("headers.id + ': ' + payload");
	        return adapter;
	    }
	
	    @MessagingGateway(defaultRequestChannel = "logChannel")
	    public interface MyGateway {
	        void sendToLogger(String data);
	
	    }
	}
    
    @Autowired
    private MyGateway gateway;
	
    @MockBean
    private LoggingHandler adapter;
    
    @Captor 
    private ArgumentCaptor<Message<String>> captor;
    
	@Test
	public void test() {
		gateway.sendToLogger("Hi");
		verify(adapter).handleMessage(captor.capture());
		assertEquals("Hi", captor.getValue().getPayload());
	}

}
