package com.example.demo.jpa;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ProjectRepositoryTest {
	
	@Autowired
	private ProjectRepository repository;
	
	@Autowired
    private TestEntityManager entityManager;
	
	@Test
	public void testSaveOneSmallProject() {
		Project project = new SmallProject();
		
		repository.save(project);

		assertNotNull(entityManager.find(SmallProject.class, project.getId()));
		assertNotNull(entityManager.find(Project.class, project.getId()));
		assertNull(entityManager.find(LargeProject.class, project.getId()));
	}
}
