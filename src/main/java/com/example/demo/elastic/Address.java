package com.example.demo.elastic;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;

@Data
public class Address {
	
	@Id
	@GeneratedValue
	private Long id;
	
	private String street;

}
