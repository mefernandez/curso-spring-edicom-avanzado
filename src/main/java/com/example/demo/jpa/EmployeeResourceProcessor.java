package com.example.demo.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.LinkBuilder;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceProcessor;
import org.springframework.stereotype.Component;

@Component
public class EmployeeResourceProcessor implements ResourceProcessor<Resource<Employee>> {

	@Autowired
	private EntityLinks entityLinks;
	
	@Override
	public Resource<Employee> process(Resource<Employee> resource) {
		Employee config = resource.getContent();

		LinkBuilder linkBuilder = entityLinks.linkForSingleResource(Employee.class, config.getId()).slash("anular");
		Link link = linkBuilder.withRel("anular");
		resource.add(link);
		return resource;
	}

}
