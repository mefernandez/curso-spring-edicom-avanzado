package com.example.demo.jpa;

import org.springframework.beans.factory.annotation.Value;

public interface EmployeeNameProjection {
	
	public String getFirstName();
	public String getLastName();
	
	@Value("#{target.firstName + ' ' + target.lastName}")
	public String getFullName();

}
