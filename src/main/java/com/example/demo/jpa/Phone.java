package com.example.demo.jpa;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
public class Phone {
	
	@Id
	@GeneratedValue
	private Long id;
	
	private String number;

	public Phone(String number) {
		super();
		this.number = number;
	}

}
