package com.example.demo.batch;

import java.util.Calendar;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.task.TaskExecutor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.jpa.Employee;
import com.example.demo.jpa.EmployeeRepository;

import lombok.Value;

@RestController
public class SpringBatchController {
	
	@Autowired
	private JobLauncher jobLauncher;
	
	@Autowired
	private JobRepository jobRepository;

	@Autowired
	private TaskExecutor taskExecutor;

	@Autowired
	@Qualifier("updateEmployeesJob")
	private Job job;
	
	@Value
	public static class JobInfo {
		Long id;
		String status;
	}

	@GetMapping("/job")
	public ResponseEntity<JobInfo> job() throws JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException, JobParametersInvalidException {
		JobParametersBuilder paramsBuilder = new JobParametersBuilder();
		paramsBuilder.addString("input.file.path", "src/test/resources/employees-invalid.csv");
		JobParameters jobParameters = paramsBuilder.toJobParameters();
		SimpleJobLauncher jobLauncher = new SimpleJobLauncher();
		jobLauncher.setTaskExecutor(taskExecutor);
		jobLauncher.setJobRepository(jobRepository);
		JobExecution jobExecution = jobLauncher.run(job, jobParameters);
		
		String status = jobExecution.getExitStatus().getExitCode();
		Long id = jobExecution.getId();
		JobInfo info = new JobInfo(id, status);
		return ResponseEntity.ok(info);
	}
	
	@Autowired
	private JobExplorer jobExplorer;
	
	@GetMapping("/job/{id}")
	public ResponseEntity<JobInfo> getJobInfo(@PathVariable Long id) throws JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException, JobParametersInvalidException {
		JobExecution jobExecution = jobExplorer.getJobExecution(id);
		if (jobExecution == null) {
			return ResponseEntity.notFound().build();
		}
		String status = jobExecution.getExitStatus().getExitCode();
		JobInfo info = new JobInfo(id, status);
		return ResponseEntity.ok(info);
	}
	
	//@Scheduled(fixedRate = 10000)
	public void launchScheduledJob() throws JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException, JobParametersInvalidException {
		JobParametersBuilder paramsBuilder = new JobParametersBuilder();
		paramsBuilder.addString("input.file.path", "src/test/resources/employees-invalid.csv");
		JobParameters jobParameters = paramsBuilder.toJobParameters();

		JobExecution jobExecution = jobLauncher.run(job, jobParameters);
	}
	


}
