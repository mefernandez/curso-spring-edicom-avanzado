package com.example.demo.integration;

import static org.junit.Assert.*;

import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.integration.annotation.Filter;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.integration.annotation.Router;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.channel.QueueChannel;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.integration.FilterTest.Configuration.MyGateway;

@RunWith(SpringRunner.class)
public class FilterTest {
	
	@TestConfiguration
	@EnableIntegration
	@IntegrationComponentScan
	public static class Configuration {
		
	    @Bean
	    public MessageChannel inputChannel() {
	        return new DirectChannel();
	    }

	    @Bean
	    public QueueChannel aChannel() {
	        return new QueueChannel();
	    }

	    @Filter(inputChannel = "inputChannel", outputChannel = "aChannel")
		public boolean filter(String input, @Header(name="header", required = false) String header) {
			return "a".equals(input);
		}
	    
	    @MessagingGateway(defaultRequestChannel = "inputChannel")
	    public interface MyGateway {
	        void filter(@Payload String data, @Header("header") String header);
	    } 
	}
	
	@Autowired
	private MessageChannel inputChannel;
	
	@Autowired
	private QueueChannel aChannel;

	@Test
	public void testPassFilter() {
		Message<String> message = MessageBuilder.withPayload("a").setHeader("header", "value").build();
		inputChannel.send(message);
		Message<String> outMessage = (Message<String>) aChannel.receive(0);
		assertEquals("a", outMessage.getPayload());
	}	

	@Autowired
	private MyGateway gateway;
	
	@Test
	public void testPassGateway() {
		gateway.filter("a", "value");
		Message<String> outMessage = (Message<String>) aChannel.receive(0);
		assertEquals("a", outMessage.getPayload());
	}	

	@Test
	public void testGateway() {
		gateway.filter("b", "value");
		Message<String> outMessage = (Message<String>) aChannel.receive(0);
		assertNull(outMessage);
	}	
	
}
