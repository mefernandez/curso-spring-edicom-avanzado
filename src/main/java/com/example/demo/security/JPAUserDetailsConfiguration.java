package com.example.demo.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

import com.example.demo.jpa.EmployeeRepository;

import lombok.AllArgsConstructor;

@Configuration
public class JPAUserDetailsConfiguration {
	
	@Autowired
	private EmployeeRepository repository;
	
	@Bean
	public UserDetailsService userDetailsService() throws Exception {
		UserDetailsService service = new JPAUserDetailsService(repository); 
		return service;
	}
	
	@AllArgsConstructor
	public static class JPAUserDetailsService implements UserDetailsService {

		@Autowired
		private EmployeeRepository repository;

		@Override
		public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
			return repository.findOneByUsername(username, UserDetails.class);
		}
		
	}

}
