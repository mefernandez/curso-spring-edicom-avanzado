package com.example.demo.jpa;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class EmployeeRepositoryTest {
	
	@Autowired
	private EmployeeRepository repository;
	
	@Autowired
    private TestEntityManager entityManager;
	
	@Test
	public void testCountEmployees() {
		assertEquals(0, repository.count());
	}

	@Test
	public void testSaveEmployee() {
		Employee employee = new Employee();
		repository.save(employee);
		
		assertNotNull(employee.getId());
	}

	@Test
	public void testUpdateEmployee() {
		Employee employee = new Employee();
		entityManager.persist(employee);
		
		employee.setFirstName("Updated");
		repository.save(employee);
		
		assertEquals("Updated", entityManager.find(Employee.class, employee.getId()).getFirstName());
	}

	@Test
	public void testSaveEmployeeWithAddress() {
		Employee employee = new Employee();
		Address address = new Address();
		employee.setAddress(address);
		
		repository.save(employee);

		assertNotNull(entityManager.find(Employee.class, employee.getId()));
		assertNotNull(entityManager.find(Address.class, address.getId()));
	}
	
	@Test
	public void testSaveEmployeeWithPhones() {
		Employee employee = new Employee();
		List<Phone> phones = Arrays.asList(new Phone("1"), new Phone("2"));
		employee.setPhones(phones);
		
		repository.save(employee);

		assertNotNull(entityManager.find(Employee.class, employee.getId()));
		assertNotNull(entityManager.find(Phone.class, phones.get(0).getId()));
		assertNotNull(entityManager.find(Phone.class, phones.get(1).getId()));
	}
	
	@Test
	public void testRemoveOnePhoneFromEmployee() {
		Employee employee = new Employee();
		List<Phone> phones = new ArrayList<>(Arrays.asList(new Phone("1"), new Phone("2")));
		employee.setPhones(phones);
		entityManager.persist(employee);
		
		employee.getPhones().remove(0);
		repository.save(employee);

		assertNotNull(entityManager.find(Employee.class, employee.getId()));
		assertEquals(1, entityManager.find(Employee.class, employee.getId()).getPhones().size());
	}

	@Test
	public void testAddSmallAndLargeProjects() {
		Project smallProject = new SmallProject();
		entityManager.persist(smallProject);
		Project largeProject = new LargeProject();
		entityManager.persist(largeProject);
		
		Employee employee = new Employee();
		employee.setProjects(Arrays.asList(largeProject, smallProject));
		repository.save(employee);

		assertNotNull(entityManager.find(Employee.class, employee.getId()));
		assertEquals(2, entityManager.find(Employee.class, employee.getId()).getProjects().size());
	}

	@Test
	public void testFindByFirstName() {
		Employee employee = new Employee();
		employee.setFirstName("Mary Jane");
		entityManager.persist(employee);
		
		assertEquals("Mary Jane", repository.findByFirstNameContainsIgnoreCase("Mary").get(0).getFirstName());
	}

	@Test
	public void testFindByAddress() {
		Employee employee = new Employee();
		employee.setFirstName("Mary Jane");
		Address address = new Address();
		address.setStreet("Baker 221B");
		employee.setAddress(address);
		entityManager.persist(employee);
		
		assertEquals("Mary Jane", repository.findByAddressStreetContainsIgnoreCase("Baker").get(0).getFirstName());
	}

	@Test
	public void testFindBySpecification() {
		{
		Employee employee = new Employee();
		employee.setFirstName("Free");
		entityManager.persist(employee);
		}
		{
		Project smallProject = new SmallProject();
		entityManager.persist(smallProject);
		Project largeProject = new LargeProject();
		entityManager.persist(largeProject);
		
		Employee employee = new Employee();
		employee.setProjects(Arrays.asList(largeProject, smallProject));
		employee.setFirstName("Busy");
		entityManager.persist(employee);
		}
		
		assertEquals("Free", repository.findAll(new FreeEmployeeSpecification()).get(0).getFirstName());
	}
	
	@Test
	public void testFindByQueryDSL() {
		{
		Employee employee = new Employee();
		employee.setFirstName("Free");
		entityManager.persist(employee);
		}
		{
		Project smallProject = new SmallProject();
		entityManager.persist(smallProject);
		Project largeProject = new LargeProject();
		entityManager.persist(largeProject);
		
		Employee employee = new Employee();
		employee.setProjects(Arrays.asList(largeProject, smallProject));
		employee.setFirstName("Busy");
		entityManager.persist(employee);
		}
		
		assertEquals("Free", repository.findAll(QEmployee.employee.projects.isEmpty()).iterator().next().getFirstName());
	}
	


	/**
	 * El campo "version" se asigna directamente al SQL 
	 * cuando acaba la transaccion, por lo que no se puede testear aquí.
	 */
	@Test
	public void cantTestOptimisticLock() {
		Long id = null;
		{
		Employee employee = new Employee();
		employee.setFirstName("Version 1");
		employee.setVersion(1);
		entityManager.persist(employee);
		id = employee.getId();
		}
		Employee employee = repository.findOne(id);
		repository.save(employee);
		assertNotEquals(2, employee.getVersion());
	}

	@Test
	public void whyCantTestOptimisticLock() {
		Long id = null;
		{
		Employee employee = new Employee();
		employee.setFirstName("Version 1");
		employee.setVersion(1);
		entityManager.persist(employee);
		id = employee.getId();
		}
		Employee employee1 = repository.findByFirstNameContainsIgnoreCase("Version 1").get(0);
		Employee employee2 = repository.findByFirstNameContainsIgnoreCase("Version 1").get(0);
		assertTrue(employee1 == employee2);
	}

	@Test
	public void testFindByCustomQuery() {
		Employee employee = new Employee();
		employee.setFirstName("Sherlock Holmes");
		Address address = new Address();
		address.setStreet("221B Baker Street");
		employee.setAddress(address);
		entityManager.persist(employee);
		
		assertEquals("221B Baker Street", repository.findByAddressCustomQuery("Baker").get(0).getAddress().getStreet());
		assertEquals("221B Baker Street", repository.findByAddressCustomNativeQuery("Baker").get(0).getAddress().getStreet());
		
	}
	
}
