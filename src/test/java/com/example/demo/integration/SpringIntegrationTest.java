package com.example.demo.integration;

import java.util.Scanner;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.MessageChannel;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = BasicIntegrationConfiguration.class)
public class SpringIntegrationTest {
	
	@Autowired
	private MessageChannel fileChannel;
	
	@Test
	public void test() {
		/*
		AbstractApplicationContext context 
	      = new AnnotationConfigApplicationContext(BasicIntegrationConfiguration.class);
	    context.registerShutdownHook();
	     
	    Scanner scanner = new Scanner(System.in);
	    System.out.print("Please enter q and press <enter> to exit the program: ");
	     
	    while (true) {
	       String input = scanner.nextLine();
	       if("q".equals(input.trim())) {
	          break;
	      }
	    }
	    System.exit(0);
	    */
		fileChannel.send(MessageBuilder.withPayload("").build());
		System.out.println(fileChannel);
	}

}
