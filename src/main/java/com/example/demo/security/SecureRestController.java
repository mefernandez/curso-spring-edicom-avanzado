package com.example.demo.security;

import java.security.Principal;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SecureRestController {
	
	@GetMapping("/secure")
	public ResponseEntity<String> secure() {
		return ResponseEntity.ok().build();
	}

	@GetMapping("/secure/user")
	public ResponseEntity<String> user(@AuthenticationPrincipal Principal principal) {
		String name = principal.getName();
		return ResponseEntity.ok(name);
	}
	
	@GetMapping("/secure/admin")
	public ResponseEntity<String> admin(@AuthenticationPrincipal Principal principal) {
		String name = principal.getName();
		return ResponseEntity.ok(name);
	}

	@GetMapping("/form")
	public ResponseEntity<String> form(@AuthenticationPrincipal Principal principal) {
		String name = principal.getName();
		return ResponseEntity.ok(name);
	}

	@PreAuthorize("hasRole('USER')")
	@GetMapping("/has-role-user")
	public ResponseEntity<String> method(@AuthenticationPrincipal Principal principal) {
		String name = principal.getName();
		return ResponseEntity.ok(name);
	}
}
