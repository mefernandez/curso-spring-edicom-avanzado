package com.example.demo.jpa;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@Entity
@Inheritance
@DiscriminatorColumn
public abstract class Project {
	
	@Id
	@GeneratedValue
	private Long id;
	
}
