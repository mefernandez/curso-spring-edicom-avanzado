package com.example.demo.security;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = SecureRestController.class)
@Import({InMemoryUserDetails.class, MultiHttpSecurityConfig.class})
public class SecureRestControllerTest {
	
	@Autowired
	private MockMvc mvc;
	
	@Test
	public void testUnauthorized() throws Exception {
		mvc.perform(
				get("/secure")
			)
			.andExpect(status().isUnauthorized());
	}

	@Test
	@WithMockUser(username = "user", roles = {"USER"})
	public void testWithMockUser() throws Exception {
		mvc.perform(
				get("/secure/user")
			)
			.andExpect(status().isOk())
			.andExpect(content().string("user"));
	}

	@Test
	@WithMockUser(username = "admin", roles = {"USER", "ADMIN"})
	public void testWithMockAdmin() throws Exception {
		mvc.perform(
				get("/secure/admin")
			)
			.andExpect(status().isOk());
	}
	
	@Test
	public void testRedirectsToFormLogin() throws Exception {
		mvc.perform(
				get("/form")
			)
			.andDo(print())
			.andExpect(status().is3xxRedirection())
			.andExpect(redirectedUrlPattern("**/login"));
	}
	
	@Test
	@WithMockUser
	public void testFormLogin() throws Exception {
		mvc.perform(
				get("/form")
			)
			.andDo(print())
			.andExpect(status().isOk());
	}
	
	@Test
	public void testWithBasicAuth() throws Exception {

		mvc.perform(
				get("/secure/user")
				.with(httpBasic("user", "password"))
			)
			.andExpect(status().isOk())
			.andExpect(content().string("user"));
		
	}
	
	@Test
	@WithMockUser
	public void testPreAuthorizeWithMockUser() throws Exception {

		mvc.perform(
				get("/has-role-user")
			)
			.andExpect(status().isOk())
			.andExpect(content().string("user"));
		
	}

	@Test
	public void testPreAuthorizeMissingUser() throws Exception {

		mvc.perform(
				get("/has-role-user")
			)
			.andExpect(status().is3xxRedirection())
			.andExpect(redirectedUrlPattern("**/login"));
		
	}
}
