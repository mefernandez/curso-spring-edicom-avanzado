package com.example.demo.jpa;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

import org.hibernate.HibernateException;
import org.hibernate.MappingException;
import org.hibernate.engine.config.spi.ConfigurationService;
import org.hibernate.engine.jdbc.connections.spi.JdbcConnectionAccess;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.id.Configurable;
import org.hibernate.id.IdentifierGenerator;
import org.hibernate.internal.util.config.ConfigurationHelper;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.type.Type;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

import lombok.extern.apachecommons.CommonsLog;

@CommonsLog
public class EmployeeIdGenerator implements IdentifierGenerator, Configurable {

	private SimpleDateFormat dateFormatter;
	    
	@Override
    public void configure(Type type, Properties params, ServiceRegistry serviceRegistry) throws MappingException {
        String datePattern = ConfigurationHelper.getString("date-format", params);
        this.dateFormatter = new SimpleDateFormat(datePattern);
    }

	@Override
	public synchronized Serializable generate(SessionImplementor session, Object object) throws HibernateException {
		Employee employee = (Employee) object;
		Long generatedValue = null;
		JdbcConnectionAccess jdbcConnectionAccess = session.getJdbcConnectionAccess();
		Connection connection = null;
		try {
			connection = jdbcConnectionAccess.obtainConnection();
			JdbcTemplate template = new JdbcTemplate(new SingleConnectionDataSource(connection, false));
			Date now = template.queryForObject("select now()", Date.class);
			String dateAsString = dateFormatter.format(now);
			generatedValue = Long.valueOf(dateAsString);
		} catch (Exception e) {
			log.error("Error generando secuencia", e);
		} finally {
			if (connection != null) {
				try {
					jdbcConnectionAccess.releaseConnection(connection);
				} catch (SQLException e) {
					log.error("Error liberando conexión JDBC al generar secuencia", e);
				}
			}
		}
		return generatedValue;
	}

}
