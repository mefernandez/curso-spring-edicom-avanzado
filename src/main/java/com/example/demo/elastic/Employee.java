package com.example.demo.elastic;

import java.util.List;

import javax.persistence.Id;

import org.springframework.data.elasticsearch.annotations.Document;

import lombok.Data;

@Data
@Document(indexName = "employees")
public class Employee {
	
	@Id
	private String id;
	
	private String firstName;

	private String lastName;

	private Address address;

	private List<Phone> phones;

}
