package com.example.demo.jpa;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@Entity
@DiscriminatorValue(value = "SP")
public class SmallProject extends Project {
	
}
