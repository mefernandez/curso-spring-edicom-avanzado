# Curso de Spring - Avanzado

## Temario

Día 1:
1. Spring Data JPA
2. Spring Data Rest

Día 2:
3. QueryDSL para consultas
4. Spring ElasticSearch

Día 3:
5. Spring Batch
6. Spring Integration

Día 4:
7. Spring Security
7.1 Basic Auth
7.2 OAuth
7.3 Spring Session


