package com.example.demo.integration;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.MessageChannel;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.integration.SimpleIntegrationFlowTest.Configuration.Upcase;

@RunWith(SpringRunner.class)
public class SimpleIntegrationFlowTest {
	
	@TestConfiguration
	@EnableIntegration
	@IntegrationComponentScan
	public static class Configuration {
		
		@Bean
		public IntegrationFlow helloWorldFlow() {
			return IntegrationFlows.from("input")
			            .filter("World"::equals)
		    			.transform("Hello "::concat)
		    			.handle(System.out::println)
		    			.get();
		}
		
		@MessagingGateway
	    public interface Upcase {
	        @Gateway(requestChannel = "upcase.input")
	        Collection<String> upcase(Collection<String> strings);
	    }
	    @Bean
	    public IntegrationFlow upcase() {
	         return f -> f
	             .split()                                         // 1
	            .<String, String>transform(String::toUpperCase)  // 2
	            .aggregate();                                    // 3
	    }
		
	}
	
	@Autowired
	@Qualifier("input")
	MessageChannel inputChannel;

	@Test
	public void testHelloWorld() {
		inputChannel.send(MessageBuilder.withPayload("World").build());
	}

	@Autowired
	private Upcase upcase;
	
	@Test
	public void testUpperCase() {
		Collection<String> upcased = upcase.upcase(Arrays.asList("hola", "mundo"));
		assertEquals("[HOLA, MUNDO]", upcased.toString());
	}
}
