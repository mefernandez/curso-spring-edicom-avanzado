package com.example.demo.jpa;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.hamcrest.Matchers.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@WithMockUser
public class EmployeeRestRespositoryTest {
	
	@Autowired
	private MockMvc mvc;
	
	@Autowired
	private EmployeeRepository repository;

	@Test
	public void test() throws Exception {
		mvc.perform(
				get("/api/employees")
				)
			.andDo(print())
			.andExpect(status().isOk())
			.andExpect(jsonPath("$._embedded.employees", hasSize(0)));
	}

	@Test
	@Transactional
	public void testLinkAnular() throws Exception {
		Employee employee = new Employee();
		repository.save(employee);
		mvc.perform(
				get("/api/employees/" + employee.getId())
				)
			.andDo(print())
			.andExpect(jsonPath("$._links.anular.href", endsWith("/employees/" + employee.getId() + "/anular")));
	}
}
