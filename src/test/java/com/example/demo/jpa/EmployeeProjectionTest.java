package com.example.demo.jpa;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class EmployeeProjectionTest {
	
	@Autowired
	private EmployeeRepository repository;
	
	@Autowired
	private TestEntityManager em;
	
	@Test
	public void testFindProjection() {
		Employee employee = new Employee();
		employee.setFirstName("Sherlock");
		employee.setLastName("Holmes");
		em.persist(employee);
		
		assertEquals("Sherlock Holmes", repository.findOneEmployeeNameProjectionById(employee.getId()).getFullName());
	}

	@Test
	public void testFindDTO() {
		Employee employee = new Employee();
		employee.setFirstName("Sherlock");
		employee.setLastName("Holmes");
		em.persist(employee);
		
		assertEquals("Sherlock Holmes", repository.findOneEmployeeNameDTOById(employee.getId()).getFullName());
	}

	@Test
	public void testFindDynamic() {
		Employee employee = new Employee();
		employee.setFirstName("Sherlock");
		employee.setLastName("Holmes");
		em.persist(employee);
		
		assertEquals("Sherlock Holmes", repository.findOneDynamicEmployeeProjectionById(employee.getId(), EmployeeNameProjection.class).getFullName());
		assertEquals("Sherlock Holmes", repository.findOneDynamicEmployeeProjectionById(employee.getId(), EmployeeNameDTO.class).getFullName());
	}
}
