package com.example.demo.jpa;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.hamcrest.Matchers.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;


@RunWith(SpringRunner.class)
@SpringBootTest
@WithMockUser(username = "testing")
public class JPAAuditTest {
	
	@Autowired
	private EmployeeRepository repository;
	
	@Test
	public void testCreatedBy() {
		Employee employee = new Employee();
		
		repository.save(employee);
		
		assertNotNull(employee.getCreatedBy());
		assertEquals("testing", employee.getCreatedBy());
	}
	
	@Test
	public void testCreatedDate() {
		Employee employee = new Employee();
		
		repository.save(employee);
		
		assertNotNull(employee.getCreatedDate());
	}	
}
