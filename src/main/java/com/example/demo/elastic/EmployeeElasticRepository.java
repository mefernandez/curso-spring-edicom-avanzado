package com.example.demo.elastic;

import java.util.List;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface EmployeeElasticRepository 
	extends ElasticsearchRepository<Employee, Long> {
	
	List<Employee> findByFirstNameContainsIgnoreCase(String firstName);

	List<Employee> findByAddressStreetContainsIgnoreCase(String street);

}
