package com.example.demo.jpa;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Version;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotBlank;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@Entity
public class Employee extends AuditableEntity {
	
	@Id
	@GeneratedValue
	/*
	@GenericGenerator(
	        name = "employee-id-generator",
	        strategy = "com.example.demo.jpa.EmployeeIdGenerator",
	        parameters = {
	            @org.hibernate.annotations.Parameter(
	                name = "date-format", value = "yyyyMMddHHmmss")
	        }
	    )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="employee-id-generator")
	*/
	private Long id;

	private String firstName;
	
	private String lastName;

	@OneToOne(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
	private Address address;

	@OneToMany(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	private List<Phone> phones;

	@ManyToMany
	private List<Project> projects;
	
	private String username;
}
