package com.example.demo.batch;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;

import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.SkipListener;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.batch.item.adapter.ItemWriterAdapter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindException;

import com.example.demo.jpa.Address;
import com.example.demo.jpa.Employee;
import com.example.demo.jpa.EmployeeRepository;
import com.example.demo.jpa.Phone;
import com.example.demo.jpa.QEmployee;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.dsl.BooleanExpression;

import lombok.AllArgsConstructor;
import lombok.extern.apachecommons.CommonsLog;

@CommonsLog
@Configuration
@EnableBatchProcessing
@EnableScheduling
public class SpringBatchConfig {
    
    @Autowired
    private JobBuilderFactory jobs;
 
    @Autowired
    private StepBuilderFactory steps;
    
    @Autowired
    private JobRepository repo;

    @Bean
    @StepScope
    public FlatFileItemReader<Employee> itemReader(@Value("file:#{jobParameters['input.file.path']}") Resource inputFile) throws UnexpectedInputException, ParseException, IOException {
    	if (!inputFile.exists()) {
    		throw new UnexpectedInputException("File " + inputFile.getFile().getAbsolutePath() + " does not exist");
    	}
        FlatFileItemReader<Employee> reader = new FlatFileItemReader<Employee>();
        reader.setResource(inputFile);
        reader.setLinesToSkip(1);

        DelimitedLineTokenizer tokenizer = new DelimitedLineTokenizer();
        String[] tokens = { "firstName", "lastName", "address", "phone" };
        tokenizer.setNames(tokens);
        
        DefaultLineMapper<Employee> lineMapper = new DefaultLineMapper<Employee>();
        lineMapper.setLineTokenizer(tokenizer);
        lineMapper.setFieldSetMapper(new EmployeeMapper());
        
        reader.setLineMapper(lineMapper);
        return reader;
    }
    
    
    public static class EmployeeMapper implements FieldSetMapper<Employee> {

        public Employee mapFieldSet(FieldSet fieldSet) throws BindException {
            Employee employee = new Employee();
			employee.setFirstName(fieldSet.readString("firstName"));
            employee.setLastName(fieldSet.readString("lastName"));
            
            Address address = new Address();
            address.setStreet(fieldSet.readString("address"));
            employee.setAddress(address);
            	
            Phone phone = new Phone();
            phone.setNumber(fieldSet.readString("phone"));
            employee.setPhones(Arrays.asList(phone));
            return employee;

        }

    }
 
    @AllArgsConstructor
    public static class EmployeeItemProcessor implements ItemProcessor<Employee, Employee> {
    	
    	private EmployeeRepository repository;

        public Employee process(Employee employee) {
        	String firstName = employee.getFirstName();
        	String lastName = employee.getLastName();
        	
			BooleanExpression predicate = QEmployee.employee.firstName.equalsIgnoreCase(firstName)
        			.and(QEmployee.employee.lastName.equalsIgnoreCase(lastName));
        	
			boolean exists = repository.exists(predicate);
			if (exists) {
				// Filter out!
				return null;
			}
			
            employee.setCreatedBy("Spring Batch");
            return employee;
        }
    }
 
    @Bean
    public ItemProcessor<Employee, Employee> itemProcessor(EmployeeRepository repository) {
        return new EmployeeItemProcessor(repository);
    }
    
    @Bean
    public ItemWriter<Employee> itemWriter(EmployeeRepository repository) {
        ItemWriterAdapter<Employee> itemWriterAdapter = new ItemWriterAdapter<Employee>();
        itemWriterAdapter.setTargetObject(repository);
        itemWriterAdapter.setTargetMethod("save");
		return itemWriterAdapter;
    }
    
    @AllArgsConstructor
    public static class MoveInputFileToTemp implements Tasklet {
    	
    	private Resource inputFile;

		@Override
		public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
			Path tempFile = Files.createTempFile("tmp_", ".tmp");
			Files.copy(inputFile.getFile().toPath(), tempFile, StandardCopyOption.REPLACE_EXISTING);
			log.info(String.format("Copied %s to %s", inputFile.getFile().getAbsolutePath(), tempFile.toString()));
			return RepeatStatus.FINISHED;
		}
    	
    }
    
    @Bean
    @StepScope
    public Tasklet tasklet(@Value("file:#{jobParameters['input.file.path']}") Resource inputFile) {
    	Tasklet tasklet = new MoveInputFileToTemp(inputFile);
    	return tasklet;
    }
    
    
    @Bean
    protected Step taskletStep(Tasklet tasklet) {
        return steps
        		.get("taskletStep")
        		.tasklet(tasklet)
        		.build();
    }    
 
    @Bean
    protected Step updateEmployees(ItemReader<Employee> reader,
      ItemProcessor<Employee, Employee> processor,
      ItemWriter<Employee> writer) {
        return steps
        		.get("updateEmployees")
        		.<Employee, Employee> chunk(10)
        		.reader(reader)
        		.processor(processor)
        		.writer(writer)
	    			.faultTolerant()
	    			.retryLimit(0)
	    			.skip(Exception.class)
	    			.skipLimit(2)
	    			.listener(new EmployeeSkipListener())
        		.build();
    }
    
    public static class EmployeeSkipListener implements SkipListener<Employee, Employee> {

		@Override
		public void onSkipInRead(Throwable t) {
			log.error("Skipped read item", t);
		}

		@Override
		public void onSkipInWrite(Employee item, Throwable t) {
			log.error("Skipped written item " + item, t);
		}

		@Override
		public void onSkipInProcess(Employee item, Throwable t) {
			log.error("Skipped processed item " + item, t);
		}
    	
    }
    
    @Bean(name = "updateEmployeesJob")
    public Job updateEmployeesJob(@Qualifier("updateEmployees") Step updateEmployees) {
        return jobs.get("updateEmployeesJob").start(updateEmployees).build();
    }

    @Bean(name = "moveInputFileJob")
    public Job moveInputFileJob(@Qualifier("taskletStep") Step taskletStep) {
        return jobs.get("moveInputFileJob").start(taskletStep).build();
    }
}
