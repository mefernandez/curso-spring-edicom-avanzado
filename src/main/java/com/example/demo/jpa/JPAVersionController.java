package com.example.demo.jpa;

import java.util.Calendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class JPAVersionController {
	
	@Autowired
	private EmployeeRepository repository;

	@GetMapping("/version")
	public ResponseEntity<Employee> version() {
		return ResponseEntity.ok(repository.save(new Employee()));
	}
	
	@GetMapping("/version/{id}")
	public ResponseEntity<Employee> getVersion(@PathVariable("id") Long id) {
		Employee employee = repository.findOne(id);
		return ResponseEntity.ok(employee);
	}


	@GetMapping("/version/update/{id}")
	public ResponseEntity<Employee> updateVersion(@PathVariable("id") Long id, @RequestParam("version") long version) {
		
		Employee employee = repository.findOne(id);
		employee.setVersion(version);
		employee.setFirstName("Updated now: " + Calendar.getInstance().getTimeInMillis());
		repository.save(employee);
		return ResponseEntity.ok(employee);
	}
	
	

}
