package com.example.demo.jpa;

import lombok.Value;

@Value
public class EmployeeNameDTO {
	
	String firstName, lastName;
	
	public String getFullName() {
		return this.firstName + " " + this.lastName;
	}

}
