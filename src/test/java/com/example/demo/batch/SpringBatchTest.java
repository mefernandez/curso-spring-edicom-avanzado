package com.example.demo.batch;


import static org.junit.Assert.*;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobInstance;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.jpa.Employee;
import com.example.demo.jpa.EmployeeRepository;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = SpringBatchConfig.class)
public class SpringBatchTest {
	
    @Autowired
    private JobLauncher jobLauncher;
    
    @Autowired
    private JobRepository jobRepository;

    @Autowired
    private JobExplorer jobExplorer;

    @Autowired
    @Qualifier("updateEmployeesJob")
    private Job updateEmployeesJob;

    @Autowired
    @Qualifier("moveInputFileJob")
    private Job moveInputFileJob;
    
    private JobLauncherTestUtils jobLauncherTestUtils = new JobLauncherTestUtils();

    @Before
    public void before() {
    	jobLauncherTestUtils.setJobLauncher(jobLauncher);
    	jobLauncherTestUtils.setJobRepository(jobRepository);
    }
    
    @MockBean
    private EmployeeRepository repository;
    
	@Test
	public void testJobInsertsEmployeesUsingJPA() throws Exception {
    	jobLauncherTestUtils.setJob(updateEmployeesJob);
		JobParametersBuilder paramsBuilder = new JobParametersBuilder();
		paramsBuilder.addString("input.file.path", "src/test/resources/employees.csv");
		JobParameters jobParams = paramsBuilder.toJobParameters();
		JobExecution jobExecution = jobLauncherTestUtils.launchJob(jobParams);
        Assert.assertEquals(ExitStatus.COMPLETED, jobExecution.getExitStatus());
        verify(repository, times(1)).save(any(Employee.class));
        
        int stepExecutionCount = jobRepository.getStepExecutionCount(jobExecution.getJobInstance(), "updateEmployees");
        assertEquals(1, stepExecutionCount);
        
		List<JobInstance> jobInstances = jobExplorer.findJobInstancesByJobName("updateEmployeesJob", 0, 1);
        assertEquals(1, jobInstances.size());
	}

	@Test
	public void testInvalidInputFile() throws Exception {
    	jobLauncherTestUtils.setJob(updateEmployeesJob);
    	when(repository.save(any(Employee.class))).thenThrow(new RuntimeException("Can not save this Employee"));
    	
		JobParametersBuilder paramsBuilder = new JobParametersBuilder();
		paramsBuilder.addString("input.file.path", "src/test/resources/employees-invalid.csv");
		JobParameters jobParams = paramsBuilder.toJobParameters();
		JobExecution jobExecution = jobLauncherTestUtils.launchJob(jobParams);
		
        Assert.assertEquals(ExitStatus.COMPLETED, jobExecution.getExitStatus());
        verify(repository, times(2)).save(any(Employee.class));
        int stepExecutionCount = jobRepository.getStepExecutionCount(jobExecution.getJobInstance(), "updateEmployees");
        assertEquals(1, stepExecutionCount);
        
		List<JobInstance> jobInstances = jobExplorer.findJobInstancesByJobName("updateEmployeesJob", 0, 1);
        assertEquals(1, jobInstances.size());
	}

	@Test
	public void testTasklet() throws Exception {
    	jobLauncherTestUtils.setJob(moveInputFileJob);
		JobParametersBuilder paramsBuilder = new JobParametersBuilder();
		paramsBuilder.addString("input.file.path", "src/test/resources/employees-invalid.csv");
		JobParameters jobParams = paramsBuilder.toJobParameters();
		
		
		JobExecution jobExecution = jobLauncherTestUtils.launchJob(jobParams);
        Assert.assertEquals(ExitStatus.COMPLETED, jobExecution.getExitStatus());
        verify(repository, times(0)).save(any(Employee.class));
        int stepExecutionCount = jobRepository.getStepExecutionCount(jobExecution.getJobInstance(), "taskletStep");
        assertEquals(1, stepExecutionCount);
        
		List<JobInstance> jobInstances = jobExplorer.findJobInstancesByJobName("moveInputFileJob", 0, 1);
        assertEquals(1, jobInstances.size());
	}
}
