package com.example.demo.integration;

import static org.junit.Assert.*;

import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.integration.annotation.Aggregator;
import org.springframework.integration.annotation.Filter;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.integration.annotation.Router;
import org.springframework.integration.annotation.Splitter;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.channel.QueueChannel;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.integration.SplitterAggregatorTest.Configuration.MyGateway;

@RunWith(SpringRunner.class)
public class SplitterAggregatorTest {
	
	@TestConfiguration
	@EnableIntegration
	@IntegrationComponentScan
	public static class Configuration {
		
	    @Bean
	    public MessageChannel inputChannel() {
	        return new DirectChannel();
	    }

	    @Bean
	    public MessageChannel aChannel() {
	        return new DirectChannel();
	    }

	    @Bean
	    public QueueChannel outputChannel() {
	        return new QueueChannel();
	    }

	    @Splitter(inputChannel = "inputChannel", outputChannel = "aChannel")
		public String[] filter(String input, @Header(name="header", required = false) String header) {
			return input.split(",");
		}
	    
	    @Aggregator(inputChannel = "aChannel", outputChannel = "outputChannel")
	    public String aggregate(String[] letters) {
	      return String.join(";", letters);
	    }

	    @MessagingGateway(defaultRequestChannel = "inputChannel", defaultReplyChannel = "outputChannel")
	    public interface MyGateway {
	        String transform(@Payload String data, @Header("header") String header);
	    } 
	}
	
	@Autowired
	private MessageChannel inputChannel;
	
	@Autowired
	private QueueChannel outputChannel;

	@Test
	public void testSplitAggregate() {
		Message<String> message = MessageBuilder.withPayload("a,b,c").setHeader("header", "value").build();
		inputChannel.send(message);
		{
		Message<String> outMessage = (Message<String>) outputChannel.receive(0);
		assertEquals("a;b;c", outMessage.getPayload());
		}
	}	
	
	@Autowired
	private MyGateway gateway;
	
	@Test
	public void testPassGateway() {
		String result = gateway.transform("a,b,c", "value");
		assertEquals("a;b;c", result);
	}	

	
}
