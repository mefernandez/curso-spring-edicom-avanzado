package com.example.demo.elastic;

import static org.elasticsearch.index.query.QueryBuilders.*;
import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EmployeeElasticRepositoryTest {
	
	@Autowired
	private EmployeeElasticRepository repository;
	
	@Autowired
	private ElasticsearchTemplate template;

	@Test
	public void testSaveEmployee() {
		Employee employee = new Employee();
		repository.save(employee);
		assertNotNull(employee.getId());
	}

	@Test
	public void testFindByFirstName() {
		Employee employee = new Employee();
		employee.setFirstName("Mary Jane");
		repository.save(employee);
		
		assertEquals("Mary Jane", repository.findByFirstNameContainsIgnoreCase("Mary").get(0).getFirstName());
	}

	@Test
	public void testFindByFullName() {
		Employee employee = new Employee();
		employee.setFirstName("Mary Jane");
		employee.setLastName("Watson");
		repository.save(employee);
		
		SearchQuery searchQuery = new NativeSearchQueryBuilder()
				.withQuery(multiMatchQuery("Mary Jane Watson", "firstName", "lastName"))
			    .build();
		
		assertEquals("Mary Jane", repository.search(searchQuery).getContent().get(0).getFirstName());
		assertEquals("Mary Jane", template.queryForList(searchQuery, Employee.class).get(0).getFirstName());
	}

}
